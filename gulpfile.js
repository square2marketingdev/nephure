
//turned off so you don't accidently upload to the default portal
var syncWithHubspot = false; 
var ftpFolder = '/portals/2659858-test-portal-www_square2marketing_com/content/templates/custom';

/* Gulp Variables */
const username = require('username');
var local = __dirname;
var directories = local.split('/');
var repo = directories[directories.length - 1];
var user = username.sync();
var scssIncludePaths = ['pages', 'modules', 'bootstrap', 'globals'];
var file = '--';

/* Node Dependencies */

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var sassIncl = require('sass-include-paths');
var run = require('gulp-run');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var gutil = require('gulp-util');
var ftp = require('vinyl-ftp');
var debug = require('gulp-debug');


/* FTP configuration */
/* This is login details for the test portal. This is currently still in alpha */
/* Meaning this doesn't work properly */

var ftpUser = 'int@square2marketing.com';
var ftpPassword = 'WGwkonaLJD7MTjSK3GSCAWNa';
var ftpHost = 'ftp.hubapi.com';
var ftpPort = '3200';
var ftpSync = ['live/**/*'];
var localFilesGlob = ['live/**/*'];
var globs = ['live/**/*'];

var customErrors = require('./errors.js');

Array.prototype.randomElement = function() {
    return this[Math.floor(Math.random() * this.length)];
}

function getFtpConnection() {
    return ftp.create({
        host: ftpHost,
        port: ftpPort,
        user: ftpUser,
        password: ftpPassword,
        parallel: 5,
        log: gutil.log,
        debug: true,
        secure: true
    });
}

function reportErrorToHipchat() {
    return run('curl -k -d \'{"color":"red","message":"'+customErrors.messages.randomElement() + ' File: '+file+' for '+repo+' updated by '+user+'","notify":false,"message_format":"text"}\' -H \'Content-Type: application/json\' https://square2.hipchat.com/v2/room/3184457/notification?auth_token=Evpekp0fOHD4JsA3YFgNqpZE6iNGrom648Ea7tFC').exec()
      .pipe(gulp.dest('output'));
}

/* This is the task that is run when you type "gulp" into the root folder */

gulp.task('default', ['watch']);

gulp.task('notify-hipchat', function() {
  return run('curl -k -d \'{"color":"green","message":"File: '+file+' for '+repo+' updated by '+user+'","notify":false,"message_format":"text"}\' -H \'Content-Type: application/json\' https://square2.hipchat.com/v2/room/3184457/notification?auth_token=Evpekp0fOHD4JsA3YFgNqpZE6iNGrom648Ea7tFC').exec()
    .pipe(gulp.dest('output'));
});

gulp.task('build-production', function() {
    var onError = function(err) {
        notify.onError({
            title: "Gulp",
            subtitle: "Error: ",
            message: "<%= error.message %>",
            sound: "Pop"
        })(err);
        this.emit('end');

    }
    return gulp.src(local+'/main.scss')
        .pipe(plumber({errorHandler: onError}))
        .pipe(rename('/'+user+'-production.scss'))
        .pipe(sass({
            includePaths: scssIncludePaths
        }))
        .pipe(gulp.dest(local));
});

gulp.task('build-development', function() {
    return gulp.src(local+'/main.scss')
        .pipe(plumber({
            handleError: function(err) {
                reportErrorToHipchat();
                this.emit('end');
            }
        }))
        .pipe(rename('/'+user+'-development.scss'))
        .pipe(sourcemaps.init())
            .pipe(sass({
                includePaths: scssIncludePaths
            }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(local));
});

gulp.task('deploy', function() {
    if(syncWithHubspot) {
        return gulp.src('./'+user+'-development.css')
            .pipe(gulp.dest('live/page/stylesheets'))
    }

});

gulp.task('watch', function() {
    if(syncWithHubspot) {
        var conn = getFtpConnection();
        var deploy = gulp.watch(localFilesGlob)
        .on('change', function(e) {
            console.log('Changes detected! Uploading file "' + e.path + '", ' + e.type);
            return gulp.src( globs, { base: 'live', buffer: false })
                .pipe(conn.newer(ftpFolder))
                .pipe(debug({ title: 'Files: ' }))
                .pipe(conn.dest(ftpFolder));
        });
    }
    var watcher = gulp.watch('**/**/*.scss', ['build-production', 'build-development', 'notify-hipchat', 'deploy']);
    watcher.on('change', function(e) {
        file = e.path.split('/');
        file = file.slice(-3).join('/');
        console.log('Building after file '+file+' was chanaged...');
    });
});
0
